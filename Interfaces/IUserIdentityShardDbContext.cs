﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using StudioKit.Data.Entity.Identity.Interfaces;
using StudioKit.Data.Entity.Identity.Models;
using StudioKit.Sharding.Entity.Identity.Models;
using StudioKit.Sharding.Entity.Interfaces;
using StudioKit.Sharding.Models;

namespace StudioKit.Sharding.Entity.Identity.Interfaces;

public interface IUserIdentityShardDbContext<TUser, TIdentityProvider, TConfiguration, TLicense>
	: IUserIdentityDbContext<TUser, TIdentityProvider>,
		IShardDbContext<TConfiguration, TLicense>
	where TUser : IdentityUser, IUser
	where TIdentityProvider : IdentityProvider, new()
	where TConfiguration : BaseShardConfiguration, new()
	where TLicense : License, new()
{
	DbSet<LicenseUser> LicenseUsers { get; set; }
}