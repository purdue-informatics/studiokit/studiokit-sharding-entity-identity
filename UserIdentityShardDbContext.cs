using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using StudioKit.Data.Entity.Identity;
using StudioKit.Data.Entity.Identity.Interfaces;
using StudioKit.Data.Entity.Identity.Models;
using StudioKit.Sharding.Entity.Identity.Interfaces;
using StudioKit.Sharding.Entity.Identity.Models;
using StudioKit.Sharding.Entity.Utils;
using StudioKit.Sharding.Models;
using System.Data.Common;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Sharding.Entity.Identity;

public class UserIdentityShardDbContext<TUser, TIdentityProvider, TConfiguration, TLicense>
	: UserIdentityDbContext<TUser, TIdentityProvider>,
		IUserIdentityShardDbContext<TUser, TIdentityProvider, TConfiguration, TLicense>
	where TUser : IdentityUser, IUser
	where TIdentityProvider : IdentityProvider, new()
	where TConfiguration : BaseShardConfiguration, new()
	where TLicense : License, new()
{
	public UserIdentityShardDbContext(DbContextOptions options) : base(options)
	{
	}

	public UserIdentityShardDbContext(DbConnection existingConnection) : base(existingConnection)
	{
	}

	public UserIdentityShardDbContext(string nameOrConnectionString) : base(nameOrConnectionString)
	{
	}

	protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
	{
		// override to use `ShardDbContextUtils` instead of `DbContextUtils`
		if (!string.IsNullOrWhiteSpace(NameOrConnectionString))
		{
			optionsBuilder
				.UseSqliteOrSqlServer(NameOrConnectionString);
		}
		else
		{
			base.OnConfiguring(optionsBuilder);
		}
	}

	protected override void OnModelCreating(ModelBuilder modelBuilder)
	{
		base.OnModelCreating(modelBuilder);

		modelBuilder.Entity<LicenseUser>(b =>
		{
			// Define compound primary key
			b.HasKey(lu => new { lu.LicenseId, lu.UserId });

			// cast IUser to TUser
			b.HasOne(lu => (TUser)lu.User);
		});
	}

	public virtual DbSet<TConfiguration> Configurations { get; set; }

	public virtual DbSet<TLicense> Licenses { get; set; }

	public virtual DbSet<LicenseUser> LicenseUsers { get; set; }

	public TConfiguration GetConfiguration()
	{
		return Configurations.OrderByDescending(c => c.Id).FirstOrDefault();
	}

	public async Task<TConfiguration> GetConfigurationAsync(CancellationToken cancellationToken = default)
	{
		return await Configurations.OrderByDescending(c => c.Id).FirstOrDefaultAsync(cancellationToken);
	}

	public TLicense GetLicense()
	{
		return Licenses.OrderByDescending(c => c.Id).FirstOrDefault();
	}

	public async Task<TLicense> GetLicenseAsync(CancellationToken cancellationToken = default)
	{
		return await Licenses.OrderByDescending(c => c.Id).FirstOrDefaultAsync(cancellationToken);
	}
}