﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Azure.SqlDatabase.ElasticScale.ShardManagement;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using StudioKit.Data.Entity.Identity.Interfaces;
using StudioKit.Data.Entity.Identity.Models;
using StudioKit.Sharding.Entity.Identity.Interfaces;
using StudioKit.Sharding.Entity.Identity.Models;
using StudioKit.Sharding.Extensions;
using StudioKit.Sharding.Models;
using StudioKit.Sharding.Utils;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Sharding.Entity.Identity;

public class UserIdentityShardManager<TContext, TConfiguration, TLicense, TUser, TIdentityProvider>
	: EntityShardManager<TContext, TConfiguration, TLicense>
	where TContext : DbContext, IUserIdentityShardDbContext<TUser, TIdentityProvider, TConfiguration, TLicense>, new()
	where TConfiguration : BaseShardConfiguration, new()
	where TLicense : License, new()
	where TUser : IdentityUser, IUser
	where TIdentityProvider : IdentityProvider, new()
{
	public UserIdentityShardManager(ILoggerFactory loggerFactory,
		ILogger<UserIdentityShardManager<TContext, TConfiguration, TLicense, TUser, TIdentityProvider>> logger) : base(loggerFactory,
		logger)
	{
	}

	protected override async Task ConfigureShardDatabaseAsync(ShardLocation location, CancellationToken cancellationToken = default)
	{
		await base.ConfigureShardDatabaseAsync(location, cancellationToken);

		if (IsConsole)
		{
			var dbContext = GetDbContext(location.GetConnectionString());
			await ConsoleAddOrUpdateIdentityProvidersAsync(dbContext, cancellationToken);
		}
	}

	private async Task ConsoleAddOrUpdateIdentityProvidersAsync(TContext dbContext, CancellationToken cancellationToken = default)
	{
		var identityProviders = await dbContext.IdentityProviders.ToListAsync(cancellationToken: cancellationToken);

		ConsoleUtils.WriteDetailedInfo("Current IdentityProviders: ");
		foreach (var identityProvider in identityProviders)
		{
			ConsoleUtils.WriteObjectProperties(identityProvider);
			Console.WriteLine();
		}

		ConsoleUpdateIdentityProviders(dbContext);

		if (dbContext.ChangeTracker.Entries<TIdentityProvider>()
			.Any(e => e.State is EntityState.Added or EntityState.Modified))
		{
			await dbContext.SaveChangesAsync(cancellationToken);

			identityProviders = await dbContext.IdentityProviders.ToListAsync(cancellationToken: cancellationToken);
			ConsoleUtils.WriteDetailedInfo("Saved IdentityProviders: ");
			foreach (var identityProvider in identityProviders)
			{
				ConsoleUtils.WriteObjectProperties(identityProvider);
				Console.WriteLine();
			}
		}
		else
		{
			ConsoleUtils.WriteDetailedInfo("No changes were made.");
		}

		Console.WriteLine();
	}

	protected virtual void ConsoleUpdateIdentityProviders(TContext context)
	{
	}

	protected override void ConsoleUpdateLicense(TLicense license)
	{
		base.ConsoleUpdateLicense(license);

		var intResponse = 0;
		while (intResponse == 0)
		{
			intResponse = ConsoleUtils.ReadIntegerInput("Set License User Limit: ", 0, _ => true);
			if (intResponse != 0)
			{
				license.UserLimit = intResponse;
			}
			else
			{
				ConsoleUtils.WriteWarning("You must enter a User Limit. Try again.");
			}
		}
	}
}