﻿using StudioKit.Data.Entity.Identity.Interfaces;
using StudioKit.Data.Interfaces;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace StudioKit.Sharding.Entity.Identity.Models;

public class LicenseUser : IAuditable
{
	public int LicenseId { get; set; }

	[ForeignKey(nameof(LicenseId))]
	public License License { get; set; }

	public string UserId { get; set; }

	[ForeignKey(nameof(UserId))]
	public IUser User { get; set; }

	public DateTime DateStored { get; set; }

	public DateTime DateLastUpdated { get; set; }

	public string LastUpdatedById { get; set; }
}